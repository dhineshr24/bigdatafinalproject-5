package com.service.createcsv;

import au.com.anthonybruno.Gen;
import au.com.anthonybruno.generator.defaults.IntGenerator;
import com.github.javafaker.Faker;
import com.util.ICreateCsv;

public class CreatePeopleCsv implements ICreateCsv {

    @Override
    public void createCsv(int noOfFilesToGenerate, String path, int noOfRowsToGenerate) {
        for (int i = 1; i <= noOfFilesToGenerate; i++) {
            Faker faker = Faker.instance();
            Gen.start()
                    .addField("Name", () -> faker.name().firstName())
                    .addField("age", new IntGenerator(18, 80))
                    .addField("phone_number", () -> faker.phoneNumber().cellPhone())
                    .addField("company", () -> faker.company().name().replaceAll(",", ""))
                    .addField("building_code", () -> faker.address().buildingNumber())
                    .addField("address", () -> faker.address().cityName())
                    .generate(noOfRowsToGenerate)
                    .asCsv()
                    .toFile(path + "people" + i + ".csv");
        }
    }

}
