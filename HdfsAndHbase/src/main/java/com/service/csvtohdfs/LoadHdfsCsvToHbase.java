package com.service.csvtohdfs;

import com.config.HbaseConfigurationFile;
import com.util.CreateHbaseTable;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class LoadHdfsCsvToHbase {
    public static void LoadToHbase(FileStatus[] status, FileSystem fs, String hbaseTableNameToCreate,
                                   ArrayList<String> columnFamilyList)
    {
        Connection connection = HbaseConfigurationFile.HbaseConfiguration();
        try {
            new CreateHbaseTable(connection, hbaseTableNameToCreate,
                    columnFamilyList).CreateHbaseTable();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int rowid = 0;
        String rowname = "";
        BufferedReader br = null;
        Table table = null;
        try {

            for (int i = 0; i < status.length; i++) {
                //Step2. Load Csv file into Hbase
                br = new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
                String line = br.readLine();
                line = br.readLine();
                table = connection.getTable(TableName.valueOf(hbaseTableNameToCreate));
                while (line != null && line.length() != 0) {
                    StringTokenizer tokens = new StringTokenizer(line, ",");
                    rowname = String.valueOf(++rowid);
                    Put p = new Put(Bytes.toBytes((rowname)));
                    p.addColumn(Bytes.toBytes(columnFamilyList.get(0)), Bytes.toBytes("name"),
                            Bytes.toBytes(tokens.nextToken()));
                    p.addColumn(Bytes.toBytes(columnFamilyList.get(0)), Bytes.toBytes("age"),
                            Bytes.toBytes(tokens.nextToken()));
                    p.addColumn(Bytes.toBytes(columnFamilyList.get(0)), Bytes.toBytes(
                            "phone_number"), Bytes.toBytes(tokens.nextToken()));
                    p.addColumn(Bytes.toBytes(columnFamilyList.get(1)), Bytes.toBytes("company"),
                            Bytes.toBytes(tokens.nextToken()));
                    p.addColumn(Bytes.toBytes(columnFamilyList.get(0)), Bytes.toBytes(
                            "building_code"),  Bytes.toBytes(tokens.nextToken()));
                    p.addColumn(Bytes.toBytes(columnFamilyList.get(0)), Bytes.toBytes("address"),
                            Bytes.toBytes(tokens.nextToken()));
                    table.put(p);
                    line = br.readLine();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("message : " + e.getMessage());
        } finally {
            try {
                br.close();
                table.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
