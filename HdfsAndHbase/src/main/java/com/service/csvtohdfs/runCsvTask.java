package com.service.csvtohdfs;

import com.config.ConfigurationFile;
import com.config.setVariables;
import com.service.createcsv.CreateBuildingCsv;
import com.service.createcsv.CreateEmployeeCsv;
import com.service.createcsv.CreatePeopleCsv;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.util.ArrayList;

import static com.service.csvtohdfs.LoadCsvToHdfs.LoadToHdfs;
import static com.service.csvtohdfs.LoadHdfsCsvToHbase.LoadToHbase;

public class runCsvTask {
    private String peopleCsvPath;
    private String employeeCsvPath;
    private String resource1;
    private String resource2;
    private String tableName;

    public runCsvTask(String peopleCsvPath, String employeeCsvPath, String resource1,
                      String resource2, String tableName) {
        this.peopleCsvPath = peopleCsvPath;
        this.employeeCsvPath = employeeCsvPath;
        this.resource1 = resource1;
        this.resource2 = resource2;
        this.tableName = tableName;
    }

    public void runTask() throws Exception {
        //Create CSV files
        runCsvTask();
        //Load all csv files to hdfs
        runHdfsTask();
        runHbaseTask();
    }

    public void runCsvTask() throws Exception {
        CreatePeopleCsv peopleCsv = new CreatePeopleCsv();
        peopleCsv.createCsv(5, peopleCsvPath, 50);

        CreateEmployeeCsv employeeCsv = new CreateEmployeeCsv();
        employeeCsv.createCsv(1, employeeCsvPath, 100);

        CreateBuildingCsv buildingCsv = new CreateBuildingCsv();
        buildingCsv.createCsv(1, employeeCsvPath, 10);
    }

    public void runHdfsTask() throws Exception {
        ConfigurationFile configuration = new ConfigurationFile(resource1, resource2);
        Configuration conf = configuration.getConfiguration();

        //Load all csv files to hdfs
        String source = setVariables.CREATEPEOPLECSVPATH;
        String destination = setVariables.PEOPLEDESTINATIONFOLDERHDFS;
        LoadToHdfs(conf, source, destination);

    }

    public void runHbaseTask() throws Exception {
        ConfigurationFile configuration = new ConfigurationFile(resource1, resource2);
        Configuration conf = configuration.getConfiguration();

        Path pt = new Path("hdfs://localhost:8020" + setVariables.PEOPLEDESTINATIONFOLDERHDFS);
        FileSystem fs = FileSystem.get(conf);
        FileStatus[] status = fs.listStatus(pt);
        ArrayList<String> columnFamilyList = new ArrayList<>();
        columnFamilyList.add("personaldata");
        columnFamilyList.add("professionaldata");
        LoadToHbase(status, fs, tableName, columnFamilyList);
    }
}


