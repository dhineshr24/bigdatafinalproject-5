package com.service.wordcount;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class WordCountDriver {
    private String inPathFiles;
    String jobName;
    private String outhPath;

    public WordCountDriver(String inPathFiles, String jobName, String outhPath) {
        this.inPathFiles = inPathFiles;
        this.jobName = jobName;
        this.outhPath = outhPath;
    }

    public int runWordCountDriver() {
        Configuration c = new Configuration();
        Path input = new Path(inPathFiles);
        Path output = new Path(outhPath);
        Job j = null;
        boolean success = false;
        try {

            j = new Job(c, jobName);
            j.setJarByClass(WordCountDriver.class);
            j.setMapperClass(MapForWordCount.class);
            j.setReducerClass(ReduceForWordCount.class);
            j.setOutputKeyClass(Text.class);
            j.setOutputValueClass(IntWritable.class);
            FileInputFormat.addInputPath(j, input);
            FileOutputFormat.setOutputPath(j, output);
            success = j.waitForCompletion(true);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success ? 0 : 1;

    }
}