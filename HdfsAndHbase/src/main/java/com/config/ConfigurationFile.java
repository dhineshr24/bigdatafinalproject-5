package com.config;

import org.apache.hadoop.conf.Configuration;

public class ConfigurationFile {
    private String resource1;
    private String resource2;

    public ConfigurationFile(String resource1, String resource2) {
        this.resource1 = resource1;
        this.resource2 = resource2;
    }

    public Configuration getConfiguration() {
        Configuration conf = new Configuration();
        conf.addResource(resource1);
        conf.addResource(resource2);
        return conf;
    }

    public Configuration getConfiguration(String resource1, String resource2) {
        Configuration conf = new Configuration();
        conf.addResource(resource1);
        conf.addResource(resource2);
        return conf;
    }

}
