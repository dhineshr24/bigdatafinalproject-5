package com.builder;

import com.config.setVariables;
import com.service.employeeleastattendance.AttendanceDriver;
import com.service.loadprototohbase.JoinBuildingEmployeeDriver;
import com.service.loadprototohbase.LoadBuildingProtoToHbase;
import com.service.loadprototohbase.LoadEmployeeProtoToHbase;
import com.service.readwriteprototohdfs.SetAttendanceProto;
import com.service.readwriteprototohdfs.SetBuildingProto;
import com.service.readwriteprototohdfs.SetEmployeeProto;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.util.ToolRunner;

public class BuilderClass {


    private String employeeCsvFilePath =
            setVariables.CREATEEMPANDBUILDINGCSVPATH + "employee1.csv";
    private String buildingCsvFilePath =
            setVariables.CREATEEMPANDBUILDINGCSVPATH + "building1.csv";

    private String attendanceStartTimestamp = "2020-04-01T10:00:00.00Z";
    private int attendanceNoOfDays = 15;
    private String buildingProtojobName = "Load Building Proto To Hbase";
    private String buildingTableName = "building";
    private String buildingProtoHbasePath = "hdfs://localhost:8020/BuildingProtoHbase";
    private String employeeProtojobName = "Load Employee Proto To Hbase";
    private String employeeTableName = "employee";
    private String employeeProtoHbasePath = "hdfs://localhost:8020/EmployeeProtoHbase";
    private String driverJobName = "Join Hbase Tables";
    private String populateCafeteriaCodeForEmployeePath = "hdfs://localhost:8020" +
            "/PopulateCafeteriaCodeForEmployee";
    private String attendanceJobName = "Lowest Attendance";
    private String attendanceTableName = "employee";
    private String attendancePath = "hdfs://localhost:8020/Attendance";
    private String employeeUri = "hdfs://localhost:8020/ProtoFiles/employee.seq";
    private String buildingUri = "hdfs://localhost:8020/ProtoFiles/building.seq";
    private String attendanceUri = "hdfs://localhost:8020/ProtoFiles/attendance.seq";

    public void buildTask(String[] args) throws Exception {
        buildProtoTask();
        buildLoadProtoToHbaseTask(args);
        buildJoinTask(args);
    }

    public void buildProtoTask() {
        SetEmployeeProto employee = new SetEmployeeProto(employeeCsvFilePath, employeeUri);
        SetBuildingProto building = new SetBuildingProto(buildingCsvFilePath,
                buildingUri);
        SetAttendanceProto attendance = new SetAttendanceProto(attendanceStartTimestamp,
                attendanceNoOfDays, attendanceUri);
        try {
            employee.setEmployeeProto();
            building.setBuildingProto();
            attendance.setAttendanceProto();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void buildLoadProtoToHbaseTask(String[] args) {
        LoadBuildingProtoToHbase buildingObj = new LoadBuildingProtoToHbase(buildingProtojobName
                , buildingUri, buildingTableName, buildingProtoHbasePath);

        LoadEmployeeProtoToHbase employeeObj = new LoadEmployeeProtoToHbase(employeeProtojobName
                , employeeUri, employeeTableName, employeeProtoHbasePath);

        try {
            ToolRunner.run(HBaseConfiguration.create(),
                    buildingObj, args);

            ToolRunner.run(HBaseConfiguration.create(),
                    employeeObj, args);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void buildJoinTask(String[] args) {
        JoinBuildingEmployeeDriver runJob = new JoinBuildingEmployeeDriver(driverJobName, buildingTableName,
                employeeTableName, populateCafeteriaCodeForEmployeePath);

        AttendanceDriver attendanceJob = new AttendanceDriver(attendanceJobName,
                attendanceTableName, attendancePath);
        try {
            runJob.run(args);
            attendanceJob.run(args);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}


